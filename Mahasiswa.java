public class Mahasiswa {
    private String nama;
    private String npm;
    private MataKuliah[] daftarMatkul;
    private int jumlahMatkul;

    public Mahasiswa(String nama, String npm) {
        this.nama = nama;
        this.npm = npm;
        this.daftarMatkul = new MataKuliah[2];
        this.jumlahMatkul = 0;
    }

    public void tambahMatkul(MataKuliah matkul) {
        if (jumlahMatkul == daftarMatkul.length) {
            tambahUkuranDaftarMatkul();
        }

        daftarMatkul[jumlahMatkul] = matkul;
        jumlahMatkul++;
    }

    private void tambahUkuranDaftarMatkul() {
        MataKuliah[] temp = new MataKuliah[daftarMatkul.length * 2];
        System.arraycopy(daftarMatkul, 0, temp, 0, daftarMatkul.length);
        daftarMatkul = temp;
    }

    public void dropMatkul(MataKuliah matkul) {
        for (int i = 0; i < jumlahMatkul; i++) {
            if (daftarMatkul[i].equals(matkul)) {
                for (int k = i + 1; k < jumlahMatkul; k++) {
                    daftarMatkul[k - 1] = daftarMatkul[k];
                }

                jumlahMatkul--;
                break;
            }
        }
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "Mahasiswa dengan nama " + nama + " dan NPM " + npm;
    }

    public String getNama() {
        return nama;
    }

    public String getNpm() {
        return npm;
    }
}

